#Wfflix handleiding 

#Database
Er zijn 2 bestanden die voor de database zijn bedoeld. Je moet 1 van de 2 gebruiken
om een goede lokale omgeving op te zetten.

#####wfflix.sql = alleen database structuur
#####wfflix-dump.sql = database structuur inclusief data

#Video files
Om de bestaande videos in de database goed werkend te krijgen moet je via
https://drive.google.com/drive/folders/1HQJBJkiYYZlCrgnhSG7dBNsT1vcRXUfG?usp=sharing
de videos downloaden.
De videos plaats je in de /assets/videos folder in de root van het project.
Indien de folder niet bestaat moet je die aanmaken.
De resultaat wordt dan:

/assets/videos/What is an API.mp4

/assets/videos/Why is Unix so important.mp4

.etc..

#Prepare server
> composer install

#Start server
> php -S localhost:9999

#Auth gegevens
| email              | password   | admin |
|--------------------|------------|-------|
| fake@wfflix.nl     | 48T03GyvcD | yes   |
| stephan@wfflix.nl  | y3ssZxqkvd | yes   |
| rudy@wfflix.nl     | VlzL09NimN | yes   |
| matthijs@wfflix.nl | 5UykUiePPU | yes   |
| john@email.com     | D37bYa8lGH | no    |

> Database login gegevens staan in de /models/BaseModel.php
