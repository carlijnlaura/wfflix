<?php

class AdminController
{

    public function index()
    {
        $admins = Admin::all();
        require 'views/admin/admins.view.php';
    }

    public function edit()
    {
        $admin = Admin::find($_GET['admin_id']);
        require 'views/admin/edit_admin.view.php';
    }

    public function editUser()
    {
        $user = User::findUser($_GET['user_id']);
        require 'views/admin/edit_user.view.php';
    }

    public function update()
    {
        $admin = Admin::find($_GET['admin_id']);
        if (!isset($_POST['is_admin'])) {
            $admin->delete();
        } else {
            if (!$admin->update($_POST)) {
                echo "nope";
                exit;
            }
        }
        header('Location: /admin/admins');
    }

    public function showUsers()
    {
        $nonadmins = User::all();
        $users = [];
        foreach ($nonadmins as $user) {
            if (!$user->isAdmin()) {
                $users[] = $user;
            }
        }
        require 'views/admin/add.view.php';
    }

    public function addUserAsAdmin()
    {
        if (isset($_POST['users'])) {
            foreach ($_POST['users'] as $userid) {
                $user = User::findUser($userid);
                $user->makeAdmin();
            }
        }
        header('Location: /admin/admins');
    }

}
