<?php

class LoginController
{

    public function index()
    {
        require 'views/login.view.php';
    }

    public function login()
    {
        $user = User::tryLogin($_POST['email'], $_POST['password']);
        if ($user) {
            $_SESSION['user'] = $user;
            header('Location: /homepage');
        } else {
            header('Location: /login?failed=true');
        }
    }

    public function logout()
    {
        session_destroy();
        header('Location: /');
    }

}
