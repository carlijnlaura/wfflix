<?php

class UserController
{

    public function view()
    {
        $temp = User::all();
        $users = [];
        foreach($temp as $t) {
            if(!$t->isAdmin()) {
                $users[] = $t;
            }
        }
        require 'views/users.view.php';
    }

    public function update()
    {
        $userModel = User::findUser($_POST['id']);
        $userModel->updateUser($_POST);
        header('Location: /admin/users');
    }

    public function delete()
    {
        $userModel = User::findUser($_POST['id']);
        if(!$userModel->deleteUser()) {
            echo "Something went wrong";
            exit;
        }
        header('Location: /admin/users');
    }

}
