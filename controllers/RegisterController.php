<?php

class RegisterController
{

    public function getRegisterView()
    {
        require 'views/register.view.php';
    }

    public function register()
    {
        if(User::checkUser($_POST['email'])) {
            //If the provided username already exists - display error.
            //Killing the script completely.
            echo "User already exist";
            exit;
        }
        $user = User::createUser($_POST);
        if ($user) {
            $_SESSION['user'] = $user;
            header('Location: /homepage');
        }
    }
}