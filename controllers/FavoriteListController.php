<?php


class FavoriteListController
{
    public function index()
    {
        $videos = Favorite::getFavoriteList($_SESSION['user']->id);
        require 'views/favoriteList.view.php';
    }
}