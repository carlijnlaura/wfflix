<?php

class VideoController
{

    public function all()
    {
        $videos = Video::allVideos();
        require 'views/admin.view.php';
    }

    public function view()
    {
        $video = Video::findVideo($_GET['video_id']);
        require 'views/video.view.php';
    }

    public function favorite()
    {
        echo Video::findVideo($_GET['video_id'])->favorite();
    }

    public function search()
    {
        $videos = Video::search($_GET['q'] ?? "");
        require 'views/search.view.php';
    }

    public function edit()
    {
        $video = Video::findVideo($_GET['id']);
        require 'views/videoEdit.view.php';
    }

    public function remove()
    {
        $videos = Video::removeVideo($_GET['id']);
        header('Location: /admin/videos');
    }

    public function update()
    {
        $video = Video::findVideo($_POST['id']);
        $video->update($_POST);
        header('Location: /admin/videos');
    }

    public function create()
    {
        if (!empty($_FILES['video'])) {
            $random = random_int(1000, 100000);
            $storePath = "/assets/videos/" . $random . ".mp4";
            $fullPath = dirname(__DIR__, 1) . "/assets/videos/" . $random . ".mp4";
            if (move_uploaded_file($_FILES['video']['tmp_name'], $fullPath)) {
                Video::addVideo([
                    'title' => $_POST['title'],
                    'description' => $_POST['description'],
                    'path' => $storePath
                ]);

                header('Location: /admin/videos');
            } else {
                echo 'Adding video has failed!';
            }
        }
    }

    public function getCreate()
    {
        require 'views/videoAdd.view.php';
    }

    public function addComment()
    {
        $comment = Comment::create([
           'message' => $_POST['message'],
           'videoid' => $_GET['video_id'],
        ]);
        if($comment) {
            require 'views/_partials/comment.view.php';
            exit;
        }
        echo "false";
    }

    public function deleteComment()
    {
        $comment = Comment::find($_GET['comment_id']);
        echo $comment->delete();
    }

}
