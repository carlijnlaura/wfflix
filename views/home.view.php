<!DOCTYPE html>
<html lang="en">
    <head>
        <title>WFFLIX</title>
        <?php require 'views/_partials/header.view.php' ?>
    </head>
    <body>
        <?php require 'views/_partials/navbar.view.php' ?>
        <div class="container" >
            <div class="text-center">
                <h1>Unlimited educational video's</h1>
                <h3>Ready to watch?</h3>
                <h5>Enter your email to create your membership.</h5>
                <form method="get" action="/register">
                <div class="input-group mt-5">
                        <input name="email" required type="email" class="form-control" placeholder="Email address">
                        <div class="input-group-append">
                            <button class="btn btn-warning"
                                    style="border-color: #ff8c00!important; color:white !important;background-color: #ff8c00;"
                                    type="submit">Let's go!
                            </button>
                        </div>
                </div>
                </form>
            </div>
        </div>
        <hr>
        <div class="container my-5">
            <div class="row">
                <div class="col-md-8 p-5">
                    <h1>Not just code</h1>
                    <h3>Learn to work with different operating systems from Windows to Unix</h3>
                </div>
                <div class="col-md-4">
                    <img src="/assets/img/terminal.png" class="img-fluid">
                </div>
            </div>
        </div>
        <hr>
        <div class="container my-5">
            <div class="row">
                <div class="col-md-4">
                    <img src="/assets/img/code.png" class="img-fluid">
                </div>
                <div class="col-md-8 p-5">
                    <h1>Work together</h1>
                    <h3>Improve your team skills</h3>
                </div>
            </div>
        </div>
        <hr>
        <div class="container my-5">
            <div class="row">
                <div class="col-md-8 p-5">
                    <h1>IDE</h1>
                    <h3>Learn tips and tricks. Here you can learn how to work with a variety of IDE's</h3>
                </div>
                <div class="col-md-4">
                    <img src="/assets/img/phpstorm.png" class="img-fluid">
                </div>
            </div>
        </div>
        <?php require '_partials/footer.view.php' ?>
    </body>
</html>