<!DOCTYPE html>
<html lang="en">
    <head>
        <title>WFFLIX - Video list</title>
        <?php require 'views/_partials/header.view.php' ?>
    </head>
    <body>
        <?php require 'views/_partials/navbar.view.php' ?>

        <div class="container">
            <div class="d-flex justify-content-between">
                Video list
                <a href="/admin/create-form" class="btn btn-sm btn-outline-primary">Add video</a>
            </div>
            <hr>
            <div class="row">
                <?php if (count($videos)):
                    foreach ($videos as $video): ?>
                        <div class="col-md-4 mb-4">
                            <div class="card cursor-pointer"
                                 onclick="window.location.href = '/video?video_id=<?= $video->id ?>'">
                                <video height="160" class="card-img-top">
                                    <source src="<?= $video->path ?>" muted
                                            preload="none" autoplay="false"
                                            type="video/mp4">
                                </video>
                                <div class="card-body">
                                    <h5 class="card-title mb-0"><?= $video->title ?></h5>
                                </div>
                                <div class="card-footer">
                                    <a class="btn btn-primary" href="/admin/edit-video?id=<?= $video->id ?>">
                                        Edit
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; endif; ?>
            </div>
        </div>

        <?php require '_partials/footer.view.php' ?>
    </body>
</html>