<!DOCTYPE html>
<html lang="en">
<head>
    <?php require 'views/_partials/header.view.php' ?>
    <title>Favorites</title>
</head>
<body>
<?php require 'views/_partials/navbar.view.php' ?>
    <div class="container" >
        <h2>My favorites</h2>
        <hr>
        <div class="row">
            <?php if(isset($_SESSION['user'])):?>
            <?php if (count($videos)):
                foreach ($videos as $video): ?>
                    <?php if($video->id !== null) { ?>
                        <div class="col-md-4 mb-4">
                        <div class="card cursor-pointer"
                             onclick="window.location.href = '/video?video_id=<?= $video->id ?>'">
                            <video height="160" class="card-img-top">
                                <source src="<?= $video->path ?>" muted
                                        preload="none" autoplay="false"
                                        type="video/mp4">
                            </video>
                            <div class="card-body">
                                <h5 class="card-title mb-0"><?= $video->title ?></h5>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                <?php endforeach;
            endif;
            endif;?>
        </div>
    </div>
<?php require '_partials/footer.view.php' ?>
</body>
</html>

