<!DOCTYPE html>
<html lang="en">
    <head>
        <title>WFFLIX - Login</title>
        <?php require 'views/_partials/header.view.php' ?>
    </head>
    <body>
        <?php require 'views/_partials/navbar.view.php' ?>
        <div class="container" >
            <h2>Login</h2>

            <div class="card w-100 my-3">
                <form action="/login-post" method="post">
                    <div class="card-body">
                        <?php if(isset($_GET['failed'])) { ?>
                            <div class="alert alert-warning" role="alert">
                                Invalid email and/or password
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" placeholder="Email" name="email">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" placeholder="Password" name="password">
                            <a href="/register">Not a member yet? Register here!</a>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-warning" type="submit">Log in</button>
                    </div>
                </form>
            </div>
        </div>

        <?php require '_partials/footer.view.php' ?>
    </body>
</html>