<!DOCTYPE html>
<html lang="en">
    <head>
        <title>WFFLIX</title>
        <?php require 'views/_partials/header.view.php' ?>
    </head>
    <body>
        <?php require 'views/_partials/navbar.view.php' ?>
        <div class="container">
            <div class="d-flex justify-content-between">
                <a href="/admin/users" class="btn btn-sm btn-outline-primary">< Back</a>
                <form action="/admin-delete" method="post">
                    <input type="hidden" name="id" value="<?= $user->id ?>">
                    <input type="submit" class="btn btn-sm btn-outline-danger w-100" value="Delete">
                </form>
            </div>
            <hr>
            <div class="card">
                <div class="card-header">
                    Edit user
                </div>
                <form method="post" action="/admin-update" role="form">
                    <div class="card-body">
                        <input type="hidden" name="id" value="<?= $user->id ?>">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" value="<?= $user->name ?>" name="name">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" value="<?= $user->email ?>" name="email">
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>

        <?php require 'views/_partials/footer.view.php' ?>
    </body>
</html>