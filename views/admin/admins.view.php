<!DOCTYPE html>
<html lang="en">
    <head>
        <title>WFFLIX</title>
        <?php require 'views/_partials/header.view.php' ?>
    </head>
    <body>
        <?php require 'views/_partials/navbar.view.php' ?>
        <div class="container">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <div>Admins</div>
                    <a href="/admin/admins/add-list" class="btn btn-sm btn-outline-primary">Add admin</a>
                </div>
                <div class="card-body">
                    <table class="table table-striped">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th width="50"></th>
                        </tr>
                        <?php foreach ($admins as $admin) { ?>
                            <tr>
                                <td><?= $admin->id ?></td>
                                <td><?= $admin->name ?></td>
                                <td><?= $admin->email ?></td>
                                <td>
                                    <a href="/admin/admins/edit?admin_id=<?= $admin->id ?>">edit</a>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>

        <?php require 'views/_partials/footer.view.php' ?>
    </body>
</html>