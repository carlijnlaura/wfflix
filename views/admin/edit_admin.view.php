<!DOCTYPE html>
<html lang="en">
    <head>
        <title>WFFLIX</title>
        <?php require 'views/_partials/header.view.php' ?>
    </head>
    <body>
        <?php require 'views/_partials/navbar.view.php' ?>
        <div class="container">
            <a href="/admin/admins" class="btn btn-sm btn-outline-primary">< Back</a>
            <hr>
            <div class="card">
                <div class="card-header">
                    Edit admin
                </div>
                <form action="/admin/admins/update?admin_id=<?= $admin->id ?>" method="POST">
                    <div class="card-body">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" value="<?= $admin->name ?>" name="name">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" value="<?= $admin->email ?>" name="email">
                        </div>
                        <?php if($admin->id !== $_SESSION['user']->id) { ?>
                            <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1" name="is_admin" checked="checked">
                            <label class="form-check-label" for="exampleCheck1">Is admin</label>
                        </div>
                        <?php } else { ?>
                            <small class="text-muted">You cannot remove yourself</small>
                            <input type="hidden" name="is_admin" value="true">
                        <?php } ?>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>

        <?php require 'views/_partials/footer.view.php' ?>
    </body>
</html>