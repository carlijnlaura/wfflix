<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Video toevoegen</title>
        <?php require 'views/_partials/header.view.php' ?>
    </head>
    <body>
        <?php require 'views/_partials/navbar.view.php' ?>
        <div class="container">
            <a href="/admin/videos" class="btn btn-sm btn-outline-primary">< Back</a>
            <hr>
            <div class="card w-100 my-3">
                <form action="/admin/create-video" method="post" enctype='multipart/form-data'>
                    <div class="card-header">
                        Add video
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" class="form-control" name="title">
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea name='description' class="form-control" id='description' cols='30'
                                      rows='5'></textarea>
                        </div>
                    </div>
                    <input style="margin-left: 24px;" name="video" type="file" id="file">
                    <br><br>
                    <div class="card-footer">
                        <button class="btn btn-primary" type="submit">Voeg toe</button>
                    </div>
                </form>
            </div>
            <?php require '_partials/footer.view.php' ?>
        </div>
    </body>
</html>
