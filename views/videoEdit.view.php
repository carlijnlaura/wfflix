<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Video wijzigen</title>
        <?php require 'views/_partials/header.view.php' ?>

    </head>
    <body>
        <?php require 'views/_partials/navbar.view.php' ?>
        <div class="container">
            <div class="d-flex justify-content-between">
                <a href="/admin/videos" class="btn btn-sm btn-outline-primary">< Back</a>
                <a class="btn btn-sm btn-danger" href="/admin/remove-video?id=<?= $video->id ?>">
                    Delete
                </a>
            </div>
            <hr>
            <div class="card">
                <form action='/admin/update-video' method="post">
                    <div class="card-header">
                        Edit video
                    </div>
                    <div class="card-body">
                        <input type='hidden' name='id' value='<?= $video->id ?>'>
                        <div class="form-group">
                            <label>Title</label>
                            <input type='text' class="form-control"
                                   value='<?= $video->title ?>' name='title'>
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea name='description' class="form-control" id='description' cols='30'
                                      rows='5'><?= $video->description ?></textarea>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type='submit' class='btn btn-primary' value='<?= $video->id ?>'>Save
                        </button>
                    </div>
            </div>
        </div>
        <?php require '_partials/footer.view.php' ?>
    </body>
</html>
