<!DOCTYPE html>
<html lang="en">
    <head>
        <title>WFFLIX - Video</title>
        <?php require 'views/_partials/header.view.php' ?>
    </head>
    <body>
        <?php require 'views/_partials/navbar.view.php' ?>
        <div class="container">
            <div class='page-header'>
                <a href="/homepage" class="btn btn-sm btn-outline-primary">< Back</a>
                <hr>
            </div>
            <video controls style="width: 100%; max-height: 60vh;min-height: 500px;" autoplay="true">
                <source src="<?= $video->path ?>" type="video/mp4">
                Your browser does not support HTML video.
            </video>
            <div class="row my-3">
                <div class="col-10">
                    <h2><?= $video->title ?></h2>
                    <p><?= $video->description ?></p>
                </div>
                <div class="col-2">
                    <a class="btn <?php if ($video->isFavorite()) {
                        echo "btn-outline-danger";
                    } else {
                        echo "btn-light";
                    } ?> w-100 favoritevideo" data-url="/video/favorite?video_id=<?= $video->id ?>">❤ <span
                                class="ml-2"><?= $video->favorites() ?></span></a>
                </div>
            </div>

            <hr>
            <p><span id="commentcount"><?= count($video->comments()) ?></span> comments</p>
            <form action="/video/add/comment?video_id=<?= $video->id ?>" method="post" class="ajaxform">
                <div class="input-group mb-3">
                    <textarea type="text" class="form-control" rows="1"
                              placeholder="Add a public comment..." name="message" required></textarea>
                    <div class="input-group-append">
                        <button class="btn btn-warning"
                                style="border-color: #ff8c00!important; color:white !important;background-color: #ff8c00;"
                                type="submit">Comment
                        </button>
                    </div>
                </div>
            </form>
            <div id="commentcontainer">
                <?php if (count($video->comments())) {
                    foreach ($video->comments() as $comment) {
                        require '_partials/comment.view.php';
                    }
                } ?>
            </div>
        </div>

        <?php require '_partials/footer.view.php' ?>
    </body>
</html>