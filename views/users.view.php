<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Admin - user information</title>
        <?php require 'views/_partials/header.view.php' ?>
    </head>
    <body>
        <?php require 'views/_partials/navbar.view.php' ?>

        <div class="container">
            <div class="card">
                <div class="card-header">Users</div>
                <div class="card-body">
                    <?php if (count($users)) { ?>
                        <table class="table table-striped">
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th width="50"></th>
                            </tr>
                            <?php foreach ($users as $user) { ?>
                                <tr>
                                    <td><?= $user->id ?></td>
                                    <td><?= $user->name ?></td>
                                    <td><?= $user->email ?></td>
                                    <td>
                                        <a href="/admin/users/edit?user_id=<?= $user->id ?>">edit</a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    <?php } else { ?>
                        <p class="mb-0">No users found</p>
                    <?php } ?>
                </div>
            </div>
        </div>

        <?php require '_partials/footer.view.php' ?>
    </body>
</html>