<!DOCTYPE html>
<html lang="en">
    <head>
        <title>WFFLIX - Video</title>
        <?php require 'views/_partials/header.view.php' ?>
    </head>
    <body>
        <?php require 'views/_partials/navbar.view.php' ?>
        <div class="container">
            <div class='page-header'>
                <a href="/homepage" class="btn btn-sm btn-outline-primary">< Back</a>
                <hr>
            </div>
            <?php if (count($videos)) { ?>
                <div class="row">
                    <?php foreach ($videos as $video) { ?>
                        <div class="col-md-4 mb-4">
                            <div class="card cursor-pointer"
                                 onclick="window.location.href = '/video?video_id=<?= $video->id ?>'">
                                <video height="160" class="card-img-top">
                                    <source src="<?= $video->path ?>" muted
                                            preload="none" autoplay="false"
                                            type="video/mp4">
                                </video>
                                <div class="card-body">
                                    <h5 class="card-title mb-0"><?= $video->title ?></h5>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } else { ?>
                <h1 class="text-center my-5">No results</h1>
            <?php } ?>
        </div>

        <?php require '_partials/footer.view.php' ?>
    </body>
</html>