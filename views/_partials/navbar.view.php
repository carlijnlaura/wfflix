<nav class="navbar navbar-expand-lg px-5 d-flex justify-content-between"
     style="box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.2); margin-bottom: 40px;">
    <div class="container">
        <?php if (isset($_SESSION['user'])) { ?>
            <a class="navbar-brand py-0" href="/homepage">
                <img src="/assets/img/WFFLIX.png" style="height: 100px">
            </a>
            <form action="/search" method="GET" class="form-inline">
                <div class="input-group">
                    <input type="search" name="q" class="form-control" placeholder="Search videos">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="submit">🔎</button>
                    </div>
                </div>
            </form>

            <?php if ($_SESSION['user']->IsAdmin()) { ?>
                <a class="nav-link" href="/admin/users">Users</a>
                <a class="nav-link" href="/admin/admins">Admins</a>
                <a class="nav-link" href="/admin/videos">Videos</a>
            <?php } ?>
            <a class="nav-link" href="/favorites">Favorites</a>
            <a class="btn btn-warning btn-xs" href="/logout">Logout</a>
        <?php } else { ?>
            <a class="navbar-brand py-0" href="/">
                <img src="/assets/img/WFFLIX.png" style="height: 100px">
            </a>
            <a class="btn btn-warning btn-xs" href="/login">Log in</a>
        <?php } ?>
    </div>
</nav>


