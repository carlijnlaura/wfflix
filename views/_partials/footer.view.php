<footer class="site-footer">
    <div class="container text-center">
        <p class="copyright-text m-0">Copyright &copy; 2020 All Rights Reserved by
            <a href="#">WFFLIX</a>
        </p>
    </div>
</footer>
<script src="/assets/js/jquery-3.5.1.min.js"></script>
<script src="/assets/js/popper.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script>
    $('textarea').on('input', function () {
        this.style.height = 'auto';
        this.style.height = (this.scrollHeight) + 'px';
    });
    $('a.favoritevideo').on('click', function (e) {
        e.preventDefault();
        let _this = $(this);
        $.ajax({
            'method': 'GET',
            'url': $(this).attr('data-url'),
            'success': function (response) {
                if (response == "true") {
                    _this.removeClass('btn-light')
                    _this.addClass('btn-outline-danger')
                    let newNumber = parseInt(_this.find('span').text()) + 1;
                    _this.find('span').text(newNumber)
                } else {
                    _this.removeClass('btn-outline-danger')
                    _this.addClass('btn-light')
                    let newNumber = parseInt(_this.find('span').text()) - 1;
                    _this.find('span').text(newNumber)
                }
            }
        })
    })
    $(document).on('click', 'a.deletecomment', function (e) {
        e.preventDefault();
        let _this = $(this);
        $.ajax({
            'method': 'GET',
            'url': $(this).attr('data-url'),
            'success': function () {
                _this.parent().parent().parent().parent().remove();
                let newCommentAmount = parseInt($('#commentcount').text()) - 1;
                $('#commentcount').text(newCommentAmount)
            }
        })
    })
    $('form.ajaxform').on('submit', function (e) {
        e.preventDefault();
        let _this = $(this)
        $.ajax({
            'method': 'POST',
            'url': $(this).attr('action'),
            'data': $(this).serialize(),
            'success': function (response) {
                if (response !== "false") {
                    $('#commentcontainer').prepend(response);
                    _this.find('textarea').val("")
                    let newCommentAmount = parseInt($('#commentcount').text()) + 1;
                    $('#commentcount').text(newCommentAmount)
                }
            }
        })
    })
</script>