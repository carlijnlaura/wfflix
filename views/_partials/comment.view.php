<div class="card my-3">
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <p><?= $comment->user()->name ?></p>
            </div>
            <div class="col-md-6 text-right">
                <?php if ($comment->userid === $_SESSION['user']->id) { ?>
                    <a data-url="/video/delete/comment?comment_id=<?= $comment->id ?>" class="btn btn-sm btn-outline-danger deletecomment text-center">
                        Delete
                    </a>
                <?php } ?>
            </div>
        </div>
        <div class="row pt-2">
            <div class="col-md-12">
                <p><?= $comment->message ?></p>
            </div>
        </div>
    </div>
</div>