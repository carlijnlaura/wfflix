<!DOCTYPE html>
<html lang="en">
    <head>
        <title>WFFLIX - Register</title>
        <?php require 'views/_partials/header.view.php' ?>
    </head>
    <body>
        <?php require 'views/_partials/navbar.view.php' ?>
        <div class="container">
            <div class="container" >
                <h2>Register</h2>

                <div class="card w-100 my-3">
                    <form action="/register-post" method="post">
                        <div class="card-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" placeholder="Name" name="name">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <?php if (isset($_GET['email'])) { ?>
                                <input type="email" class="form-control" placeholder="Email" name="email" value="<?php echo $_GET['email'] ?>">
                                <?php }else { ?>
                                <input type="email" class="form-control" placeholder="Email" name="email">
                                <?php } ?>
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" placeholder="Password" name="password">
                                <a href="/login">Already have an account? Login here!</a>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-warning" type="submit">Register</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php require '_partials/footer.view.php' ?>
    </body>
</html>