-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema wfflix
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema wfflix
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `wfflix` DEFAULT CHARACTER SET utf8 ;
USE `wfflix` ;

-- -----------------------------------------------------
-- Table `wfflix`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wfflix`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `email` VARCHAR(55) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wfflix`.`admin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wfflix`.`admin` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userid` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_admin_users_idx` (`userid` ASC) VISIBLE,
  CONSTRAINT `fk_admin_users`
    FOREIGN KEY (`userid`)
    REFERENCES `wfflix`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wfflix`.`videos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wfflix`.`videos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `description` VARCHAR(500) NOT NULL,
  `path` VARCHAR(255) NOT NULL,
  `adminid` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_videos_admin1_idx` (`adminid` ASC) VISIBLE,
  CONSTRAINT `fk_videos_admin1`
    FOREIGN KEY (`adminid`)
    REFERENCES `wfflix`.`admin` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wfflix`.`favorites`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wfflix`.`favorites` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userid` INT NOT NULL,
  `videoid` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_favorites_users1_idx` (`userid` ASC) VISIBLE,
  INDEX `fk_favorites_videos1_idx` (`videoid` ASC) VISIBLE,
  CONSTRAINT `fk_favorites_users1`
    FOREIGN KEY (`userid`)
    REFERENCES `wfflix`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_favorites_videos1`
    FOREIGN KEY (`videoid`)
    REFERENCES `wfflix`.`videos` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `wfflix`.`comments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `wfflix`.`comments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `message` VARCHAR(500) NOT NULL,
  `userid` INT NOT NULL,
  `videoid` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_comments_users1_idx` (`userid` ASC) VISIBLE,
  INDEX `fk_comments_videos1_idx` (`videoid` ASC) VISIBLE,
  CONSTRAINT `fk_comments_users1`
    FOREIGN KEY (`userid`)
    REFERENCES `wfflix`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_comments_videos1`
    FOREIGN KEY (`videoid`)
    REFERENCES `wfflix`.`videos` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
