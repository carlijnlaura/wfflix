# ************************************************************
# Sequel Ace SQL dump
# Version 2082
#
# https://sequel-ace.com/
# https://github.com/Sequel-Ace/Sequel-Ace
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.5.5-MariaDB)
# Database: wfflix
# Generation Time: 2020-10-20 13:51:32 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table admin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_admin_users_idx` (`userid`),
  CONSTRAINT `fk_admin_users` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;

INSERT INTO `admin` (`id`, `userid`)
VALUES
	(1,1),
	(2,2),
	(3,3),
	(4,4);

/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(500) NOT NULL,
  `userid` int(11) NOT NULL,
  `videoid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_comments_users1_idx` (`userid`),
  KEY `fk_comments_videos1_idx` (`videoid`),
  CONSTRAINT `fk_comments_users1` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_comments_videos1` FOREIGN KEY (`videoid`) REFERENCES `videos` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;

INSERT INTO `comments` (`id`, `message`, `userid`, `videoid`)
VALUES
	(1,'tincidunt lacus at velit vivamus vel nulla eget eros elementum pellentesque quisque porta',3,5),
	(2,'duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum',2,3),
	(3,'eget elit sodales scelerisque mauris sit amet eros suspendisse accumsan tortor quis turpis sed ante vivamus tortor duis mattis egestas metus',1,8),
	(4,'ultrices phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget orci',3,11),
	(5,'in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat id mauris vulputate elementum nullam varius nulla facilisi',1,6),
	(6,'nulla ultrices aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est',2,14),
	(7,'aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque viverra pede ac',2,15),
	(8,'sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod',1,9),
	(9,'sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl',5,14),
	(10,'ultricies eu nibh quisque id justo sit amet sapien dignissim vestibulum vestibulum ante ipsum primis in faucibus orci luctus',3,13),
	(11,'id turpis integer aliquet massa id lobortis convallis tortor risus dapibus augue vel accumsan tellus',2,11),
	(12,'duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo rhoncus sed',3,9),
	(13,'dictumst etiam faucibus cursus urna ut tellus nulla ut erat id mauris vulputate elementum nullam varius nulla facilisi',4,2),
	(14,'donec odio justo sollicitudin ut suscipit a feugiat et eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut',3,15),
	(15,'nisi volutpat eleifend donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa',4,8),
	(16,'nunc rhoncus dui vel sem sed sagittis nam congue risus semper porta',5,5),
	(17,'justo morbi ut odio cras mi pede malesuada in imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum',1,18),
	(18,'donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer',4,12),
	(19,'aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est',2,6),
	(20,'massa tempor convallis nulla neque libero convallis eget eleifend luctus ultricies eu nibh quisque id justo sit amet sapien dignissim vestibulum vestibulum ante ipsum primis in faucibus orci luctus',4,10),
	(21,'tempus semper est quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae',5,11),
	(22,'sed tristique in tempus sit amet sem fusce consequat nulla nisl nunc nisl duis bibendum felis sed interdum',2,15),
	(23,'eu magna vulputate luctus cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur',2,13),
	(24,'non pretium quis lectus suspendisse potenti in eleifend quam a odio in hac habitasse platea dictumst maecenas',4,2),
	(25,'a suscipit nulla elit ac nulla sed vel enim sit amet nunc viverra dapibus nulla suscipit ligula in lacus curabitur at ipsum ac tellus semper',4,10),
	(26,'varius integer ac leo pellentesque ultrices mattis odio donec vitae nisi nam ultrices libero non mattis pulvinar nulla pede ullamcorper augue a suscipit',4,8),
	(27,'bibendum felis sed interdum venenatis turpis enim blandit mi in porttitor pede justo eu',4,18),
	(28,'auctor gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet justo morbi ut odio cras mi pede malesuada in imperdiet et',1,14),
	(29,'tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat volutpat in',5,1),
	(30,'ultrices libero non mattis pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet',1,18),
	(31,'vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum mauris non ligula pellentesque ultrices phasellus id sapien',3,16),
	(32,'porttitor lorem id ligula suspendisse ornare consequat lectus in est risus auctor sed tristique in tempus sit amet sem fusce consequat nulla',2,6),
	(33,'nam ultrices libero non mattis pulvinar nulla pede ullamcorper augue',3,11),
	(34,'nec nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus vel nulla eget eros elementum pellentesque quisque porta',2,14),
	(35,'amet turpis elementum ligula vehicula consequat morbi a ipsum integer a nibh in quis justo maecenas rhoncus aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo',3,1),
	(36,'pede malesuada in imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin interdum',5,6),
	(37,'dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere',1,13),
	(38,'volutpat erat quisque erat eros viverra eget congue eget semper rutrum nulla nunc purus phasellus in',1,7),
	(39,'odio justo sollicitudin ut suscipit a feugiat et eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat sapien',1,17),
	(40,'aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est',5,13),
	(41,'cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula nec sem duis',5,17),
	(42,'pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis nam congue risus semper porta volutpat quam pede lobortis ligula sit amet eleifend pede libero quis orci nullam molestie nibh',4,18),
	(43,'lacinia eget tincidunt eget tempus vel pede morbi porttitor lorem id ligula suspendisse ornare consequat lectus in est risus auctor sed tristique in',2,16),
	(44,'ultrices enim lorem ipsum dolor sit amet consectetuer adipiscing elit proin',1,16),
	(45,'pulvinar lobortis est phasellus sit amet erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu mi',4,12),
	(46,'faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend donec ut',5,8),
	(47,'ultrices libero non mattis pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac nulla',3,9),
	(48,'eget eros elementum pellentesque quisque porta volutpat erat quisque erat eros viverra eget congue eget semper rutrum nulla nunc purus phasellus',5,1),
	(49,'ante vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend donec ut',3,7),
	(50,'nec nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus vel nulla eget',5,14),
	(51,'est et tempus semper est quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum',5,17),
	(52,'rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis nam congue risus',1,16),
	(53,'in tempus sit amet sem fusce consequat nulla nisl nunc nisl duis bibendum felis sed interdum venenatis turpis enim blandit mi',2,18),
	(54,'id luctus nec molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est quam',3,9),
	(55,'quis orci eget orci vehicula condimentum curabitur in libero ut',5,2),
	(56,'sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie',1,6),
	(57,'vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum',3,14),
	(58,'rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id lobortis convallis tortor',3,12),
	(59,'turpis donec posuere metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit amet diam in magna bibendum imperdiet nullam orci pede venenatis non sodales',1,3),
	(60,'ante vel ipsum praesent blandit lacinia erat vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia',1,10),
	(61,'consequat morbi a ipsum integer a nibh in quis justo maecenas rhoncus aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus',4,15),
	(62,'at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget orci vehicula condimentum curabitur in libero ut massa volutpat convallis morbi odio odio',5,12),
	(63,'justo morbi ut odio cras mi pede malesuada in imperdiet et commodo vulputate justo in blandit ultrices enim lorem ipsum dolor sit',5,3),
	(64,'congue risus semper porta volutpat quam pede lobortis ligula sit amet eleifend pede libero quis orci nullam molestie nibh in lectus pellentesque at nulla suspendisse',2,16),
	(65,'fusce consequat nulla nisl nunc nisl duis bibendum felis sed interdum venenatis turpis enim blandit mi in porttitor pede justo eu massa donec dapibus',1,11),
	(66,'porttitor lacus at turpis donec posuere metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit amet diam',5,7),
	(67,'cubilia curae duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus',2,8),
	(68,'vivamus metus arcu adipiscing molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci',1,5),
	(69,'erat nulla tempus vivamus in felis eu sapien cursus vestibulum proin eu mi nulla ac enim in tempor turpis nec euismod',2,12),
	(70,'quis justo maecenas rhoncus aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio',2,17),
	(71,'nam ultrices libero non mattis pulvinar nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet nunc',3,3),
	(72,'interdum in ante vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia',3,11),
	(73,'convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus',4,5),
	(74,'nulla ultrices aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque viverra pede ac diam cras pellentesque',3,8),
	(75,'velit nec nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus vel',4,7),
	(76,'magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi',2,7),
	(77,'accumsan felis ut at dolor quis odio consequat varius integer ac leo pellentesque ultrices mattis odio donec vitae nisi nam',3,11),
	(78,'quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae mauris viverra diam',1,16),
	(79,'luctus rutrum nulla tellus in sagittis dui vel nisl duis ac nibh fusce lacus purus aliquet at feugiat non pretium quis lectus suspendisse potenti in eleifend quam',3,2),
	(80,'in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus',4,15),
	(81,'in faucibus orci luctus et ultrices posuere cubilia curae mauris viverra diam vitae quam',3,5),
	(82,'nam nulla integer pede justo lacinia eget tincidunt eget tempus vel pede morbi porttitor lorem id',1,10),
	(83,'lorem integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia eget tincidunt eget tempus',5,3),
	(84,'nisl nunc rhoncus dui vel sem sed sagittis nam congue risus semper porta volutpat quam pede lobortis ligula sit amet eleifend pede libero quis orci nullam',2,2),
	(85,'sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia eget tincidunt eget tempus vel pede morbi porttitor',4,4),
	(86,'ligula vehicula consequat morbi a ipsum integer a nibh in quis justo maecenas rhoncus aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo',2,4),
	(87,'lacus purus aliquet at feugiat non pretium quis lectus suspendisse potenti in eleifend quam a odio in hac habitasse platea dictumst maecenas ut massa quis augue luctus',1,10),
	(88,'dapibus augue vel accumsan tellus nisi eu orci mauris lacinia sapien quis libero nullam sit amet turpis elementum ligula vehicula consequat morbi a ipsum integer a nibh in quis justo',5,2),
	(89,'accumsan tortor quis turpis sed ante vivamus tortor duis mattis egestas metus aenean fermentum donec ut mauris eget massa tempor convallis nulla neque libero',2,15),
	(90,'urna ut tellus nulla ut erat id mauris vulputate elementum nullam varius nulla facilisi cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus at velit vivamus vel',3,16),
	(91,'quam sapien varius ut blandit non interdum in ante vestibulum ante ipsum primis',4,9),
	(92,'lobortis convallis tortor risus dapibus augue vel accumsan tellus nisi eu orci mauris lacinia sapien quis',3,9),
	(93,'sit amet lobortis sapien sapien non mi integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus',4,9),
	(94,'iaculis diam erat fermentum justo nec condimentum neque sapien placerat',3,5),
	(95,'faucibus cursus urna ut tellus nulla ut erat id mauris vulputate elementum nullam varius nulla facilisi cras non velit nec',2,3),
	(96,'convallis nulla neque libero convallis eget eleifend luctus ultricies eu nibh quisque id justo sit',1,1),
	(97,'non mauris morbi non lectus aliquam sit amet diam in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu felis fusce',4,9),
	(98,'blandit mi in porttitor pede justo eu massa donec dapibus duis at velit eu est congue elementum',5,7),
	(99,'augue aliquam erat volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat id mauris vulputate',1,5),
	(100,'nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet justo morbi ut odio cras',3,13),
	(101,'nullam orci pede venenatis non sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel sem',3,10),
	(102,'duis bibendum felis sed interdum venenatis turpis enim blandit mi in porttitor pede justo eu massa donec dapibus duis at velit eu est',5,6),
	(103,'ultrices posuere cubilia curae duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend',2,17),
	(104,'at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis',4,5),
	(105,'dolor vel est donec odio justo sollicitudin ut suscipit a feugiat et eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet',3,14),
	(106,'duis at velit eu est congue elementum in hac habitasse platea dictumst morbi',1,14),
	(107,'auctor gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet justo',5,4),
	(108,'erat id mauris vulputate elementum nullam varius nulla facilisi cras non velit nec nisi vulputate nonummy maecenas tincidunt',2,8),
	(109,'diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus sapien ut',1,2),
	(110,'volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet erat nulla',5,16),
	(111,'duis aliquam convallis nunc proin at turpis a pede posuere nonummy integer non velit donec diam neque vestibulum eget vulputate ut ultrices vel augue vestibulum ante ipsum primis in faucibus',4,15),
	(112,'sodales scelerisque mauris sit amet eros suspendisse accumsan tortor quis turpis sed ante vivamus tortor duis mattis egestas metus aenean fermentum donec ut mauris eget massa tempor',3,2),
	(113,'ut suscipit a feugiat et eros vestibulum ac est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien',2,1),
	(114,'elit proin risus praesent lectus vestibulum quam sapien varius ut blandit non interdum in ante vestibulum ante ipsum primis in faucibus',1,11),
	(115,'sapien placerat ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris sit amet eros suspendisse accumsan tortor quis turpis sed ante',3,14),
	(116,'natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean auctor gravida sem praesent',2,16),
	(117,'pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus sapien ut',3,12),
	(118,'tellus semper interdum mauris ullamcorper purus sit amet nulla quisque arcu libero rutrum',3,5),
	(119,'nulla ultrices aliquet maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est',1,8),
	(120,'erat vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo',2,18),
	(121,'nulla tempus vivamus in felis eu sapien cursus vestibulum proin',2,3),
	(122,'luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi',3,4),
	(123,'montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel',1,8),
	(124,'mi nulla ac enim in tempor turpis nec euismod scelerisque quam turpis adipiscing lorem vitae mattis nibh ligula nec sem duis aliquam',5,18),
	(125,'justo pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus sapien ut nunc',2,13),
	(126,'in faucibus orci luctus et ultrices posuere cubilia curae mauris viverra diam vitae quam suspendisse potenti nullam porttitor lacus at turpis donec posuere metus vitae ipsum aliquam non',3,10),
	(127,'ligula in lacus curabitur at ipsum ac tellus semper interdum mauris',1,11),
	(128,'blandit mi in porttitor pede justo eu massa donec dapibus duis at velit eu est congue elementum in hac habitasse platea dictumst morbi vestibulum velit id pretium',2,5),
	(129,'est lacinia nisi venenatis tristique fusce congue diam id ornare imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat volutpat',2,9),
	(130,'nullam orci pede venenatis non sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus',4,4),
	(131,'tristique in tempus sit amet sem fusce consequat nulla nisl nunc nisl duis bibendum felis sed interdum venenatis turpis enim blandit mi in porttitor pede justo eu massa',5,5),
	(132,'congue eget semper rutrum nulla nunc purus phasellus in felis donec semper sapien a libero nam dui proin leo odio porttitor id',4,5),
	(133,'aliquam non mauris morbi non lectus aliquam sit amet diam in magna bibendum imperdiet nullam orci pede venenatis non sodales sed tincidunt eu',5,5),
	(134,'faucibus orci luctus et ultrices posuere cubilia curae duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi volutpat eleifend donec ut dolor morbi vel lectus in',4,16),
	(135,'dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus etiam vel augue vestibulum rutrum rutrum neque aenean',3,16),
	(136,'eu magna vulputate luctus cum sociis natoque penatibus et magnis dis',4,1),
	(137,'pretium nisl ut volutpat sapien arcu sed augue aliquam erat volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna ut',3,7),
	(138,'volutpat eleifend donec ut dolor morbi vel lectus in quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit amet',4,12),
	(139,'in faucibus orci luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien non mi integer ac neque',2,14),
	(140,'lorem integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum sed magna at nunc commodo',4,10),
	(141,'magna vulputate luctus cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis dis parturient',3,11),
	(142,'lectus vestibulum quam sapien varius ut blandit non interdum in ante vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae',1,18),
	(143,'integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus in sagittis dui vel nisl duis ac nibh fusce lacus purus aliquet at feugiat',5,17),
	(144,'augue aliquam erat volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam faucibus cursus urna ut tellus nulla ut erat id mauris vulputate elementum',3,1),
	(145,'neque libero convallis eget eleifend luctus ultricies eu nibh quisque id justo sit amet sapien dignissim vestibulum vestibulum ante ipsum primis in faucibus orci luctus et',4,18),
	(146,'gravida sem praesent id massa id nisl venenatis lacinia aenean sit amet justo morbi ut odio cras mi pede malesuada in imperdiet et commodo vulputate justo in',5,12),
	(147,'in quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id lobortis convallis tortor risus dapibus augue vel',1,2),
	(148,'sapien placerat ante nulla justo aliquam quis turpis eget elit sodales scelerisque mauris sit amet eros suspendisse accumsan tortor quis turpis',2,4),
	(149,'nulla pede ullamcorper augue a suscipit nulla elit ac nulla sed vel enim sit amet nunc viverra dapibus nulla suscipit ligula in lacus curabitur at ipsum ac tellus semper interdum',4,10),
	(150,'hac habitasse platea dictumst morbi vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat ante nulla justo aliquam quis',1,5);

/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table favorites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `favorites`;

CREATE TABLE `favorites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `videoid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_favorites_users1_idx` (`userid`),
  KEY `fk_favorites_videos1_idx` (`videoid`),
  CONSTRAINT `fk_favorites_users1` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_favorites_videos1` FOREIGN KEY (`videoid`) REFERENCES `videos` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `favorites` WRITE;
/*!40000 ALTER TABLE `favorites` DISABLE KEYS */;

INSERT INTO `favorites` (`id`, `userid`, `videoid`)
VALUES
	(1,2,3),
	(2,1,3),
	(3,4,12),
	(4,5,13),
	(5,2,10),
	(6,3,7),
	(7,2,15),
	(8,3,7),
	(9,3,5),
	(10,2,3),
	(11,2,10),
	(12,3,3),
	(13,5,4),
	(14,3,5),
	(15,3,10),
	(16,3,7),
	(17,3,8),
	(18,3,15),
	(19,5,6),
	(20,3,6),
	(21,2,7),
	(22,1,6),
	(23,2,2),
	(24,1,1),
	(25,2,1);

/*!40000 ALTER TABLE `favorites` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `email` varchar(55) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `email`, `password`)
VALUES
	(1,'Fake','fake@wfflix.nl','$2y$10$OFLFL0fmt4ra1H5kDspu.u/uArrq1v9TEOb54nw4JEKB/6BabgK6W'),
	(2,'Stephan','stephan@wfflix.nl','$2y$10$W9rU45kDtOIlpMZWHKcFm.qrRC/ANPE.1KKMVYKb/4ZxVKleejuXe'),
	(3,'Rudy','rudy@wfflix.nl','$2y$10$iz.ofo1TtMGr8vHnPz.NfuOYD2s7ByEvjFJzIjB28PUFgbvwAmzRa'),
	(4,'Matthijs','matthijs@wfflix.nl','$2y$10$OOPEfNLObpNKXdwCLYZ9X.tiQo0isRmKZeO1w0D4pL/NUnpMC3AdW'),
	(5,'John','john@email.com','$2y$10$KRxioome4xuTruXu3wQ6yuIr5JH1z6cZrksvosGTEHBm1ZIfSulV2');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table videos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `videos`;

CREATE TABLE `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `description` varchar(500) NOT NULL,
  `path` varchar(255) NOT NULL,
  `adminid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_videos_admin1_idx` (`adminid`),
  CONSTRAINT `fk_videos_admin1` FOREIGN KEY (`adminid`) REFERENCES `admin` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;

INSERT INTO `videos` (`id`, `title`, `description`, `path`, `adminid`)
VALUES
	(1,'Android Tutorial For Beginners','In this video we will discuss Android Tutorial For Beginners','/assets/videos/9 Android Tutorial For Beginners _ User Interface OverView.mp4',1),
	(2,'Bootstrap 3 tutorial for beginners','In this video we will discuss Bootstrap 3 tutorial for beginners','/assets/videos/1_ Bootstrap 3 tutorial for beginners - Learn Bootstrap 3 front-end programming.mp4',2),
	(3,'How to Get Started With JavaScript','In this video we will discuss How to Get Started With JavaScript','/assets/videos/1_ How to Get Started With JavaScript _ JavaScript Tutorial Beginners _ Learn JavaScript Programming.mp4',3),
	(4,'Computer Basics - Understanding OS','In this video we will discuss Computer Basics - Understanding OS','/assets/videos/Computer Basics_ Understanding Operating Systems.mp4',4),
	(5,'Git Tutorial 8 - .gitignore file','In this video we will discuss Git Tutorial 8 - .gitignore file','/assets/videos/Git Tutorial 8 - .gitignore file.mp4',3),
	(6,'Java Tutorial for Beginners (2020)','In this video we will discuss Java Tutorial for Beginners (2020)','/assets/videos/Java Tutorial for Beginners - Introduction [Episode 1] (2020).mp4',1),
	(7,'Move form without border C#','In this video we will discuss Move form without border C#','/assets/videos/Move form without border _ C.mp4',2),
	(8,'MySQL Workbench Tutorial (2020)','In this video we will discuss MySQL Workbench Tutorial (2020)','/assets/videos/MySQL Workbench Tutorial For Beginners (2020).mp4',3),
	(9,'New in Laravel 5.5 Custom exception','In this video we will discuss New in Laravel 5.5 Custom exception','/assets/videos/New in Laravel 5.5_ Custom exception report method (6_16).mp4',4),
	(10,'Object Oriented PHP 2 - Access Modifiers','In this video we will discuss Object Oriented PHP 2 - Access Modifiers','/assets/videos/Object Oriented PHP 2 _ Access Modifiers.mp4',3),
	(11,'PHP Applications Profiling in PhpStorm','In this video we will discuss PHP Applications Profiling in PhpStorm','/assets/videos/PHP Applications Profiling in PhpStorm - PhpStorm Video Tutorial.mp4',1),
	(12,'React Render Tutorial - 1 - Introduction','In this video we will discuss React Render Tutorial - 1 - Introduction','/assets/videos/React Render Tutorial - 1 - Introduction.mp4',2),
	(13,'What is Software Architecture','In this video we will discuss What is Software Architecture','/assets/videos/What is Software Architecture_.mp4',3),
	(14,'What is an API','In this video we will discuss What is an API','/assets/videos/What is an API_.mp4',4),
	(15,'Why is Unix so Important','In this video we will discuss Why is Unix so Important','/assets/videos/Why is Unix so Important_.mp4',3),
	(16,'Ajax Tutorial','In this video we will discuss Ajax Tutorial','/assets/videos/y2mate.com - Ajax Tutorial  For Beginners_v240P.mp4',1),
	(17,'Android Tutorial','In this video we will discuss Android Tutorial','/assets/videos/y2mate.com - Android Tutorial_v240P.mp4',2),
	(18,'Delphi Programming','In this video we will discuss Delphi Programming','/assets/videos/y2mate.com - Delphi Programming Tutorial - Lesson 4_ Rapid Application Development_360p.mp4',3);

/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
