<?php

$routes = [
    'GET' => [
        '' => [
            'controller' => 'HomeController',
            'method' => 'index'
        ],
        'login' => [
            'controller' => 'LoginController',
            'method' => 'index'
        ],
        'video' => [
            'controller' => 'VideoController',
            'method' => 'view',
            'auth' => true,
        ],
        'admin/remove-video' => [
            'controller' => 'VideoController',
            'method' => 'remove',
            'auth' => true,
            'admin' => true,
        ],
        'video/delete/comment' => [
            'controller' => 'VideoController',
            'method' => 'deleteComment',
            'auth' => true,
        ],
        'admin/admins' => [
            'controller' => 'AdminController',
            'method' => 'index',
            'auth' => true,
            'admin' => true,
        ],
        'admin/admins/edit' => [
            'controller' => 'AdminController',
            'method' => 'edit',
            'auth' => true,
            'admin' => true,
        ],
        'admin/users/edit' => [
            'controller' => 'AdminController',
            'method' => 'editUser',
            'auth' => true,
            'admin' => true,
        ],
        'search' => [
            'controller' => 'VideoController',
            'method' => 'search',
            'auth' => true,
        ],
        'video/favorite' => [
            'controller' => 'VideoController',
            'method' => 'favorite',
            'auth' => true,
        ],
        'register' => [
            'controller' => 'RegisterController',
            'method' => 'getRegisterView'
        ],
        'homepage' => [
            'controller' => 'HomepageController',
            'method' => 'index',
            'auth' => true,
        ],
        'admin/admins/add-list' => [
            'controller' => 'AdminController',
            'method' => 'showUsers',
            'auth' => true,
            'admin' => true,
        ],
        'admin/videos' => [
            'controller' => 'VideoController',
            'method' => 'all',
            'auth' => true,
            'admin' => true,
        ],
        'admin/edit-video' => [
            'controller' => 'VideoController',
            'method' => 'edit',
            'auth' => true,
            'admin' => true,
        ],
        'admin/create-form' => [
            'controller' => 'VideoController',
            'method' => 'getCreate',
            'auth' => true,
            'admin' => true,
        ],
        'favorites' => [
            'controller' => 'FavoriteListController',
            'method' => 'index',
            'auth' => true,
        ],
        'admin/users' => [
            'controller' => 'UserController',
            'method' => 'view',
            'auth' => true,
            'admin' => true,
        ],
        'logout' => [
            'controller' => 'LoginController',
            'method' => 'logout'
        ],
    ],
    'POST' => [
        'video/add/comment' => [
            'controller' => 'VideoController',
            'method' => 'addComment',
            'auth' => true,
        ],
        'login-post' => [
            'controller' => 'LoginController',
            'method' => 'login'
        ],
        'register-post' => [
            'controller' => 'RegisterController',
            'method' => 'register'
        ],
        'admin-update' => [
            'controller' => 'UserController',
            'method' => 'update',
            'auth' => true,
            'admin' => true,
        ],
        'admin-delete' => [
            'controller' => 'UserController',
            'method' => 'delete',
            'auth' => true,
            'admin' => true,
        ],
        'admin/update-video' => [
            'controller' => 'VideoController',
            'method' => 'update',
            'auth' => true,
            'admin' => true,
        ],
        'admin/remove-video' => [
            'controller' => 'VideoController',
            'method' => 'remove'
        ],
        'admin/admins/update' => [
            'controller' => 'AdminController',
            'method' => 'update',
            'auth' => true,
            'admin' => true,
        ],

        'admin/admins/add' => [
            'controller' => 'AdminController',
            'method' => 'addUserAsAdmin',
            'auth' => true,
            'admin' => true,
        ],
        'admin/create-video' => [
            'controller' => 'VideoController',
            'method' => 'create',
            'auth' => true,
            'admin' => true,
        ],
    ],
];
