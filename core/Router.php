<?php

class Router
{

    public static function start() //Static function is zodat je een function kan aanroepen zonder dat je een class aanhebt gemaakt.
    {
        require 'routes.php'; //we hebben routes.php nodig in dit document
        $route = trim(explode('?', $_SERVER['REQUEST_URI'])[0], '/');

        if (array_key_exists($route, $routes[$_SERVER['REQUEST_METHOD']])) { //array_key_exists — Checks if the given key or index exists in the array
            if(isset($routes[$_SERVER['REQUEST_METHOD']][$route]["auth"]) && !isset($_SESSION['user'])) {
                header('Location: /login');
                exit;
            }
            if(isset($routes[$_SERVER['REQUEST_METHOD']][$route]["admin"]) && !$_SESSION['user']->isAdmin()) {
                header('Location: /homepage');
                exit;
            }
            $controller = new $routes[$_SERVER['REQUEST_METHOD']][$route]['controller'](); //Ik ga wat doen met de controller.
            $controller->{$routes[$_SERVER['REQUEST_METHOD']][$route]['method']}(); //Functie in de controller uitvoeren
            exit;
        }

        echo '404 Page not found';
        exit;
    }

}