<?php

class Favorite extends BaseModel
{

    public $id;
    public $userid;
    public $videoid;

    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->userid = $data['userid'];
        $this->videoid = $data['videoid'];
    }

    public static function find($id)
    {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("SELECT * WHERE id = :id LIMIT 1");
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            return new Favorite($stmt->fetch());
        }
        return null;
    }

    public static function create($video_id)
    {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("INSERT INTO favorites (userid, videoid) VALUES (:user_id, :video_id)");
        $stmt->bindParam(':user_id', $_SESSION['user']->id, PDO::PARAM_INT);
        $stmt->bindParam(':video_id', $video_id, PDO::PARAM_INT);
        if ($stmt->execute()) {
            return Favorite::find($pdo->lastInsertId());
        }
        return false;
    }

    public static function countFavorites($video_id)
    {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("SELECT videoid FROM favorites WHERE videoid = :video_id");
        $stmt->bindParam(':video_id', $video_id, PDO::PARAM_INT);
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            return $stmt->rowCount();
        }
        return 0;
    }

    public static function findFavoriteByUser($user_id, $video_id)
    {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("SELECT * FROM favorites WHERE videoid = :video_id AND userid = :user_id");
        $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $stmt->bindParam(':video_id', $video_id, PDO::PARAM_INT);
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            return new Favorite($stmt->fetch());
        }
        return false;
    }

    public function delete()
    {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("DELETE FROM favorites WHERE id = :id");
        $stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            return true;
        }
        return false;
    }

    public static function getFavoriteList($user_id)
    {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("SELECT videos.id, favorites.userid, videos.title, videos.description, videos.path FROM favorites LEFT JOIN videos ON videos.id=favorites.videoid WHERE favorites.userid = :userid");
        $stmt->bindParam(':userid', $user_id, PDO::PARAM_INT);
        $arr = [];
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            foreach ($stmt->fetchAll() as $video) {
                $arr[] = new Video($video);
            }
        }
        return $arr;
    }

}