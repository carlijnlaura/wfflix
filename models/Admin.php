<?php

class Admin extends BaseModel
{

    public $id;
    public $name;
    public $email;

    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->name = $data['name'];
        $this->email = $data['email'];
    }

    public static function all()
    {
        $users = User::all();
        $admins = [];
        foreach ($users as $user) {
            if ($user->isAdmin()) {
                /**
                 ** get_object_vars() = pak de data van de user als array
                 ** om opnieuw te kunnen gebruiken in de admin model
                 **/
                $admins[] = new Admin(get_object_vars($user));
            }
        }
        return $admins;
    }

    public static function find($id)
    {
        return new Admin(get_object_vars(User::findUser($id)));
    }

    public function update($data)
    {
        $user = User::findUser($this->id);
        return $user->updateUser($data);
    }

    public function delete()
    {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("DELETE FROM admin WHERE userid = :userid");
        $stmt->bindParam(':userid', $this->id, PDO::PARAM_INT);
        return $stmt->execute();
    }

}