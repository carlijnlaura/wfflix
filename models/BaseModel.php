<?php

class BaseModel
{

    public static function connect()
    {
        try {
            return new PDO('mysql:host=127.0.0.1;port=3306;dbname=wfflix', 'root', 'root', [
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            ]);
        } catch (PDOException $ignored) {
            echo 'Database connection failed';
            exit;
        }
    }

}