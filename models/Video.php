<?php

class Video extends BaseModel
{

    public $id;
    public $title;
    public $path;
    public $description;

    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->title = $data['title'];
        $this->path = $data['path'];
        $this->description = $data['description'];
    }

    public static function findVideo($id)
    {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("SELECT * FROM videos WHERE id = :id LIMIT 1");
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            return new Video($stmt->fetch());
        }
        return null;
    }

    public function update($data)
    {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("UPDATE videos SET title = :title, description = :description WHERE id = :id");
        $stmt->bindParam(':id', $this->id, PDO::PARAM_INT); // bind de id
        $stmt->bindParam(':title', $data['title'], PDO::PARAM_STR);
        $stmt->bindParam(':description', $data['description'], PDO::PARAM_STR);
        return $stmt->execute();
    }

    public static function removeVideo($id)
    {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("DELETE FROM videos WHERE id = :id");
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        return $stmt->execute();
    }

    public static function allVideos()
    {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("SELECT * FROM videos");
        $arr = [];
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            foreach ($stmt->fetchAll() as $video) {
                $arr[] = new Video($video);
            }
        }
        return $arr;
    }

    public function favorites()
    {
        echo Favorite::countFavorites($this->id);
    }

    public function comments()
    {
        return Comment::findByVideo($this->id);
    }

    public function isFavorite()
    {
        return (bool)Favorite::findFavoriteByUser($_SESSION['user']->id, $this->id);
    }

    public function favorite()
    {
        $favorite = Favorite::findFavoriteByUser($_SESSION['user']->id, $this->id);
        if ($favorite) {
            $favorite->delete();
            return "false";
        }
        Favorite::create($this->id);
        return "true";
    }

    public static function search($query)
    {
        $query = "%$query%";
        $pdo = parent::connect();
        $stmt = $pdo->prepare("SELECT * FROM videos WHERE title LIKE :title");
        $stmt->bindParam(':title', $query, PDO::PARAM_STR);
        $arr = [];
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            foreach ($stmt->fetchAll() as $video) {
                $arr[] = new Video($video);
            }
        }
        return $arr;
    }

    public static function getallvideos() //Roep functie getallvideos aan.
    {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("select * from videos"); // hier bereiden we het sql statement voor.
        $arr = [];
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            foreach ($stmt->fetchAll() as $video) {
                $arr[] = new Video($video);
            }
            return $arr;
        }
        return []; //return nothing
    }

    public static function addVideo($data)
    {
        $pdo = parent::connect();
        // If file has uploaded successfully, store its name in data base
        $stmt = $pdo->prepare("INSERT INTO videos (title, description, path, adminid) VALUES (:title, :description, :path, :adminid)");
        $stmt->bindParam(':title', $data['title']); // bind de id
        $stmt->bindParam(':path', $data['path']); // bind de id
        $stmt->bindParam(':description', $data['description']); // bind de id
        $stmt->bindParam(':adminid', $_SESSION['user']->id); // bind de id
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            return true;
        }
        return false;
    }

}