<?php

class Comment extends BaseModel
{

    public $id;
    public $message;
    public $userid;
    public $videoid;

    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->message = $data['message'];
        $this->userid = $data['userid'];
        $this->videoid = $data['videoid'];
    }

    public static function find($id)
    {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("SELECT * FROM comments WHERE id = :id LIMIT 1");
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            return new Comment($stmt->fetch());
        }
        return null;
    }

    public static function all()
    {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("SELECT * FROM comments");
        $arr = [];
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            foreach ($stmt->fetchAll() as $video) {
                $arr[] = new Comment($video);
            }
        }
        return $arr;
    }

    public static function create($data)
    {
        $pdo = parent::connect();
        if ($stmt = $pdo->prepare("INSERT INTO comments (message, userid, videoid) VALUES (:message, :userid, :videoid)")) {
            $stmt->bindParam(':message', $data['message']); // bind de id
            $stmt->bindParam(':videoid', $data['videoid']); // bind de id
            $stmt->bindParam(':userid', $_SESSION['user']->id); // bind de id

            if ($stmt->execute()) {
                return self::find($pdo->lastInsertId());
            }
        }
        return false;
    }

    public static function findByVideo($id)
    {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("SELECT * FROM comments where videoid = :videoid");
        $stmt->bindParam(':videoid', $id); // bind de id
        $arr = [];
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            foreach ($stmt->fetchAll() as $video) {
                $arr[] = new Comment($video);
            }
        }
        return $arr;
    }

    public function user()
    {
        return User::findUser($this->userid);
    }

    public function delete() {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("DELETE FROM comments WHERE id = :id");
        $stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
        return $stmt->execute();
    }

}