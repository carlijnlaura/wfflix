<?php

class User extends BaseModel
{

    public $id;
    public $name;
    public $email;

    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->name = $data['name'];
        $this->email = $data['email'];
    }

    public static function all()
    {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("SELECT id, name, email FROM users");
        $arr = [];
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            foreach ($stmt->fetchAll() as $item) {
                $arr[] = new User($item);
            }
        }
        return $arr;
    }

    public static function findUser($id)
    {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("SELECT id, name, email FROM users WHERE id = :id LIMIT 1"); //search if user exist with send email
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            return new User($stmt->fetch()); //fetch the found user
        }
        return null; //return nothing
    }

    public static function tryLogin($email, $password)
    {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("SELECT * FROM users WHERE email= :email LIMIT 1"); //search if user exist with sent email
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            $user = $stmt->fetch(); //fetch the found user
            if (password_verify($password, $user['password'])) { //verify user password with send password
                return new User($user); //if true return the user
            } else {
                return false;
            }
        }
    }

    public function IsAdmin()
    {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("SELECT * FROM admin where userid= :userid LIMIT 1");
        $stmt->bindValue(':userid', $this->id);
        if ($stmt->execute() && $stmt->rowCount() > 0) {
            return true;
        }
        return false;
    }

    public static function createUser($data)
    {
        $pdo = parent::connect();
        $passwordHash = password_hash($data['password'], PASSWORD_BCRYPT);
        $stmt = $pdo->prepare("INSERT INTO users (name, email, password) VALUES (:name, :email, :password)");

        $stmt->bindValue(':name', $data['name']);
        $stmt->bindValue(':email', $data['email']);
        $stmt->bindValue(':password', $passwordHash);

        //Execute the statement and insert the new account.
        $result = $stmt->execute();
        //If the signup process is successful.
        if ($result) {
            //creating new user
            mail($data['email'], 'bevestiging', 'dit is een bevestiging email van wfflix');
            return User::findUser($pdo->lastInsertId()); //finding user with the last inserted id that pdo returns
        } else { //If the signup process is wrong
            return false;
        }
    }

    public static function checkUser($email)
    {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("SELECT COUNT(email) AS num FROM users WHERE email = :email"); //check if user exist with the email
        //Bind the provided email to our prepared statement.
        $stmt->bindValue(':email', $email);
        //Execute.
        if ($stmt->execute()) {
            $data = $stmt->fetch();
            if($data['num'] > 0) {
                return true;
            }
        }
        return false;
        //if nothing found, just continue the script
    }

    public function updateUser($data)
    {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("UPDATE users SET email = :email, name = :name WHERE id= :id"); //update user statement
        $stmt->bindValue(':email', $data['email']);
        $stmt->bindValue(':name', $data['name']);
        $stmt->bindValue(':id', $this->id);
        return $stmt->execute();
    }

    public function deleteUser()
    {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("DELETE FROM users WHERE id= :id"); //update user statement
        $stmt->bindValue(':id', $this->id);
        return $stmt->execute();
    }

    public function makeAdmin()
    {
        $pdo = parent::connect();
        $stmt = $pdo->prepare("INSERT INTO admin (userid) VALUES (:userid)");
        $stmt->bindValue(':userid', $this->id);
        return $stmt->execute();
    }

}